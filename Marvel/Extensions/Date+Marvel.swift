//
//  Date+Marvel.swift
//  Marvel
//
//  Created by woroninb on 25/05/2018.
//  Copyright © 2018 com.roche. All rights reserved.
//

import Foundation

extension Date {
    func toString( dateFormat format: String ) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}
