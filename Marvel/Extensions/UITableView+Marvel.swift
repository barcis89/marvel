//
//  UITableView+Marvel.swift
//  Marvel
//
//  Created by woroninb on 25/05/2018.
//  Copyright © 2018 com.roche. All rights reserved.
//

import UIKit

extension UITableView {
    // swiftlint:disable force_cast
    func dequeueReusableCell<T: UITableViewCell>(_ type: T.Type) -> T {
        return self.dequeueReusableCell(withIdentifier: NSStringFromClass(type)) as! T
    }
    // swiftlint:enable force_cast
    func registerCell<T: UITableViewCell>(_ type: T.Type) {
        self.register(type, forCellReuseIdentifier: NSStringFromClass(type))
    }

    func registerHeaderFooterView<T: UITableViewHeaderFooterView>(_ type: T.Type) {
        self.register(type, forHeaderFooterViewReuseIdentifier: NSStringFromClass(type))
    }
}
