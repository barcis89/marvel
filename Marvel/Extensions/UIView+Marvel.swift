//
//  UIView+Marvel.swift
//  Marvel
//
//  Created by woroninb on 26/05/2018.
//  Copyright © 2018 com.roche. All rights reserved.
//

import UIKit

extension UIView {
    class func constrained() -> Self {
        let view = self.init(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
}
