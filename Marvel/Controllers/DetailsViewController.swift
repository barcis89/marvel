//
//  DetailsViewController.swift
//  Marvel
//
//  Created by woroninb on 26/05/2018.
//  Copyright © 2018 com.roche. All rights reserved.
//

import UIKit

enum DetailsSections: Int {
    case about
    case comics
    case stories
    case series

    public var description: String {
        switch self {
        case .about:
            return "About"
        case .comics:
            return "Comics"
        case .stories:
            return "Stories"
        case .series:
            return "Series"
        }
    }

    static let allValues = [about, comics, stories, series]
}

final class DetailsViewController: UIViewController {

    // UI
    fileprivate var headerImageView = UIImageView.constrained()
    fileprivate var tableView = UITableView.constrained()

    //Variables
    let maxItemsInSection = 3
    var item: Hero?

    override func viewDidLoad() {
        super.viewDidLoad()

        title = MVLStrings.MarvelDetailsVC.title
        view.backgroundColor = .white

        setupView()
        configureConstraints()
    }

    private func setupView() {

        view.backgroundColor = .white

        headerImageView.contentMode = .scaleAspectFit

        guard let item = item else {
            return
        }

        Webservice().loadImage(resource: item.media, completion: { (image) in
            DispatchQueue.main.async {
                self.headerImageView.image = image
            }
        })

        tableView.tableFooterView = UIView()
        tableView.registerCell(UITableViewCell.self)
        tableView.dataSource = self

        view.addSubview(headerImageView)
        view.addSubview(tableView)
    }

    private func configureConstraints() {

        var constraints: [NSLayoutConstraint] = []

        constraints.append(headerImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor))
        constraints.append(headerImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor))
        constraints.append(headerImageView.topAnchor.constraint(equalTo: view.topAnchor))
        constraints.append(headerImageView.heightAnchor.constraint(equalToConstant: 180))

        constraints.append(tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor))
        constraints.append(tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor))
        constraints.append(tableView.topAnchor.constraint(equalTo: headerImageView.bottomAnchor))
        constraints.append(tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor))

        NSLayoutConstraint.activate(constraints)
    }
}

// MARK: UITableViewDataSource

extension DetailsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return DetailsSections.allValues.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        guard let sectionName = DetailsSections(rawValue: section) else {
            return 0
        }

        guard let item = item else {
            return 0
        }

        switch sectionName {
        case .about:
            return item.description == "" ? 0 : 1
        case .comics:
            return item.comics.count > maxItemsInSection ? maxItemsInSection : item.comics.count
        case .series:
            return item.series.count > maxItemsInSection ? maxItemsInSection : item.series.count
        case .stories:
            return item.stories.count > maxItemsInSection ? maxItemsInSection : item.stories.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(UITableViewCell.self)

        cell.textLabel?.numberOfLines = 3
        cell.selectionStyle = .none

        guard let sectionName = DetailsSections(rawValue: indexPath.section) else {
            return UITableViewCell()
        }

        guard let item = item else {
            return UITableViewCell()
        }

        switch sectionName {
        case .about:
            cell.textLabel?.text = item.description
        case .comics:
            let comic = item.comics[indexPath.row]
            cell.textLabel?.text = comic.name
            cell.detailTextLabel?.text = comic.resourceURI
        case .series:
            let series = item.series[indexPath.row]
            cell.textLabel?.text = series.name
            cell.detailTextLabel?.text = series.resourceURI
        case .stories:
            let stories = item.stories[indexPath.row]
            cell.textLabel?.text = stories.name
            cell.detailTextLabel?.text = stories.resourceURI
        }

        return cell
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {

        guard let sectionName = DetailsSections(rawValue: section) else {
            return nil
        }

        guard let item = item else {
            return nil
        }

        switch sectionName {
        case .about:
            return item.description == "" ? nil : sectionName.description
        case .comics:
            return item.comics.isEmpty ? nil : sectionName.description
        case .series:
            return item.series.isEmpty ? nil : sectionName.description
        case .stories:
            return item.stories.isEmpty ? nil : sectionName.description
        }
    }
}
