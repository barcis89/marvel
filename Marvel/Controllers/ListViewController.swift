//
//  ViewController.swift
//  Marvel
//
//  Created by woroninb on 23/05/2018.
//  Copyright © 2018 com.roche. All rights reserved.
//

import UIKit

final class ListViewController: UIViewController {

    // UI
    fileprivate var indicator = UIActivityIndicatorView()
    fileprivate var tableView = UITableView.constrained()
    fileprivate var searchBar = UISearchBar.constrained()

    // Variables
    fileprivate var inSearchMode = false
    fileprivate var total: Int = 0
    fileprivate var offset = Constants.APIDetails.offset
    fileprivate var shouldShowLoadingCell = false

    fileprivate var items: [Hero] = [] {
        didSet {
            tableView.reloadData()
            indicator.stopAnimating()
        }
    }

    fileprivate var filteredItems: [Hero] = [] {
        didSet {
            tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = MVLStrings.MarvelListVC.title

        loadItems()

        setupView()
        configureConstraints()
    }

    fileprivate func loadItems(searchName: String? = nil) {

        Webservice().load(resource: Hero.all, offset: offset, searchName: searchName) { (data) in

            DispatchQueue.main.async {
                guard let heroesTotal = data else {
                    return
                }

                let allItems = heroesTotal.items
                self.total = heroesTotal.total

                if searchName != nil {
                    self.filteredItems = allItems
                } else {
                    self.items.append(contentsOf: allItems)
                }
            }
        }
    }

    private func setupView() {

        view.backgroundColor = .white

        searchBar.delegate = self
        searchBar.placeholder = MVLStrings.MarvelListVC.searchPlaceholder
        searchBar.returnKeyType = .done

        indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        indicator.activityIndicatorViewStyle = .gray
        indicator.center = view.center

        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerCell(HeroUITableViewCell.self)

        view.addSubview(tableView)
        view.addSubview(indicator)
        view.addSubview(searchBar)

        indicator.startAnimating()
    }

    private func configureConstraints() {

        var constraints: [NSLayoutConstraint] = []

        constraints.append(searchBar.leadingAnchor.constraint(equalTo: view.leadingAnchor))
        constraints.append(searchBar.trailingAnchor.constraint(equalTo: view.trailingAnchor))
        constraints.append(searchBar.heightAnchor.constraint(equalToConstant: 44))
        constraints.append(searchBar.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor))

        constraints.append(tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor))
        constraints.append(tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor))
        constraints.append(tableView.topAnchor.constraint(equalTo: searchBar.bottomAnchor))
        constraints.append(tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor))

        NSLayoutConstraint.activate(constraints)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// MARK: UITableViewDataSource

extension ListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return inSearchMode ? filteredItems.count : items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.row == items.count - 1 {

            let loadingCell = LoadingUITableViewCell(style: .default, reuseIdentifier: String(describing: LoadingUITableViewCell.self))

            if total > items.count {
                offset += Constants.APIDetails.offsetDelta
                loadItems()
            }

            return loadingCell

        } else {

            let cell = tableView.dequeueReusableCell(HeroUITableViewCell.self)
            let item = inSearchMode ? filteredItems[indexPath.row] : items[indexPath.row]

            cell.nameLabel.text = item.name
            cell.tag = indexPath.row
            Webservice().loadImage(resource: item.media, completion: { (image) in
                DispatchQueue.main.async {
                    if cell.tag == indexPath.row {
                        cell.thumbnailImageView.image = image
                    }
                }
            })
            return cell
        }
    }
}

// MARK: UITableViewDelegate

extension ListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let item = inSearchMode ? filteredItems[indexPath.row] : items[indexPath.row]

        let detailsVC = DetailsViewController()
        detailsVC.item = item
        show(detailsVC, sender: self)
    }
}

// MARK: UISearchBarDelegate

extension ListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == "" {
            inSearchMode = false
            view.endEditing(true)
            tableView.reloadData()
        } else {
            inSearchMode = true
            loadItems(searchName: searchText)
        }
    }
}
