//
//  Resource.swift
//  Marvel
//
//  Created by woroninb on 23/05/2018.
//  Copyright © 2018 com.roche. All rights reserved.
//

import Foundation

struct Resource<A> {
    let url: URL
    let queryItems: [NSURLQueryItem]
    let parse: (Data) -> A?
}

extension Resource {
    init(url: URL, queryItems: [NSURLQueryItem], parseJSON: @escaping (Any) -> A?) {

        self.url = url
        self.queryItems = queryItems
        self.parse = { data in
            let json = try? JSONSerialization.jsonObject(with: data as Data, options: [])
            return json.flatMap(parseJSON)
        }
    }
}
