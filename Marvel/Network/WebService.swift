//
//  WebService.swift
//  Marvel
//
//  Created by woroninb on 23/05/2018.
//  Copyright © 2018 com.roche. All rights reserved.
//

import UIKit

final class Webservice {

    func load<A>(resource: Resource<A>, offset: Int = 0, searchName: String?, completion: @escaping (A?) -> Void) {

        let urlComps = NSURLComponents(string: resource.url.absoluteString)
        urlComps?.queryItems = resource.queryItems as [URLQueryItem]
        urlComps?.queryItems?.append(URLQueryItem(name: "offset", value: String(offset)))

        if let searchName = searchName {
            urlComps?.queryItems?.append(URLQueryItem(name: "nameStartsWith", value: searchName))
        }

        guard let URL = urlComps?.url else {
            return
        }

        let request = URLRequest(url: URL)

        URLSession.shared.dataTask(with: request) { (data, _, _) in
            guard let data = data else {
                completion(nil)
                return
            }
            completion(resource.parse(data))
        }.resume()
    }

    func loadImage<A>(resource: Resource<A>, completion: @escaping (UIImage?) -> Void) {

        URLSession.shared.dataTask(with: resource.url) { data, _, _ in

            guard let imageUnWrapped = data else {
                completion(nil)
                return
            }
            let image = UIImage(data: imageUnWrapped)
            completion(image)
        }.resume()
    }
}
