//
//  HeroUITableViewCell.swift
//  Marvel
//
//  Created by woroninb on 25/05/2018.
//  Copyright © 2018 com.roche. All rights reserved.
//

import UIKit

final class HeroUITableViewCell: UITableViewCell {

    let nameLabel = UILabel.constrained()
    let thumbnailImageView = UIImageView.constrained()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        selectionStyle = .none
        accessoryType = .disclosureIndicator

        nameLabel.textColor = .black
        nameLabel.textAlignment = .left
        nameLabel.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.medium)

        thumbnailImageView.contentMode = .scaleAspectFit

        addSubview(nameLabel)
        addSubview(thumbnailImageView)

        configureConstraints()
    }

    private func configureConstraints() {

        var constraints: [NSLayoutConstraint] = []

        constraints.append(nameLabel.leadingAnchor.constraint(equalTo: thumbnailImageView.trailingAnchor, constant: 8))
        constraints.append(nameLabel.trailingAnchor.constraint(equalTo: trailingAnchor))
        constraints.append(nameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 20))
        constraints.append(nameLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20))

        constraints.append(thumbnailImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8))
        constraints.append(thumbnailImageView.widthAnchor.constraint(equalToConstant: 40))
        constraints.append(thumbnailImageView.heightAnchor.constraint(equalToConstant: 40))
        constraints.append(thumbnailImageView.centerYAnchor.constraint(equalTo: centerYAnchor))

        NSLayoutConstraint.activate(constraints)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
