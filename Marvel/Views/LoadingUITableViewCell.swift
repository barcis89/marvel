//
//  LoadingUITableViewCell.swift
//  Marvel
//
//  Created by woroninb on 26/05/2018.
//  Copyright © 2018 com.roche. All rights reserved.
//

import UIKit

final class LoadingUITableViewCell: UITableViewCell {

    let activityIndicator = UIActivityIndicatorView.constrained()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.activityIndicatorViewStyle = .gray
        activityIndicator.hidesWhenStopped = true

        activityIndicator.startAnimating()

        contentView.addSubview(activityIndicator)

        configureConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }

    private func configureConstraints() {

        var constraints: [NSLayoutConstraint] = []

        constraints.append(activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor))
        constraints.append(activityIndicator.topAnchor.constraint(equalTo: topAnchor, constant: 10))
        constraints.append(activityIndicator.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10))

        NSLayoutConstraint.activate(constraints)
    }
}
