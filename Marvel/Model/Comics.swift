//
//  Comic.swift
//  Marvel
//
//  Created by woroninb on 27/05/2018.
//  Copyright © 2018 com.roche. All rights reserved.
//

import Foundation

struct Comics {
    let name: String
    let resourceURI: String
}

extension Comics {
    init?(dictionary: JSONDictionary) {
        guard let name = dictionary["name"] as? String else { return nil }
        guard let resourceURI = dictionary["resourceURI"] as? String else { return nil }

        self.name = name
        self.resourceURI = resourceURI
    }
}
