//
//  Story.swift
//  Marvel
//
//  Created by woroninb on 27/05/2018.
//  Copyright © 2018 com.roche. All rights reserved.
//

import Foundation

struct Story {
    let type: String
    let name: String
    let resourceURI: String
}

extension Story {
    init?(dictionary: JSONDictionary) {
        guard let type = dictionary["type"] as? String else { return nil }
        guard let name = dictionary["name"] as? String else { return nil }
        guard let resourceURI = dictionary["resourceURI"] as? String else { return nil }

        self.type = type
        self.name = name
        self.resourceURI = resourceURI
    }
}
