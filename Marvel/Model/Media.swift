//
//  Media.swift
//  Marvel
//
//  Created by woroninb on 27/05/2018.
//  Copyright © 2018 com.roche. All rights reserved.
//

import UIKit

struct Media {
    let image: UIImage
}
