//
//  Hero.swift
//  Marvel
//
//  Created by woroninb on 23/05/2018.
//  Copyright © 2018 com.roche. All rights reserved.
//

import Foundation
import CryptoSwift

private let url = URL(string: MVLPlistDataProvider.url())?.appendingPathComponent(Constants.APIDetails.APIPath)
private let timestampHash = MVLPlistDataProvider.timestampHash()
private let queryItems = [NSURLQueryItem(name: Constants.APIDetails.APIKey,
                                 value: MVLPlistDataProvider.apiKey()),
                  NSURLQueryItem(name: Constants.APIDetails.Hash,
                                 value: timestampHash.hash),
                  NSURLQueryItem(name: Constants.APIDetails.TimeStamp,
                                 value: timestampHash.timestamp)]

typealias JSONDictionary = [String: Any]

struct Hero {
    let id: Int
    let name: String
    let description: String
    let thumbnailPath: String
    let thumbnailExtension: String
    let stories: [Story]
    let series: [Series]
    let comics: [Comics]
}

extension Hero {
    init?(dictionary: JSONDictionary) {
        guard let id = dictionary["id"] as? Int else { return nil }
        guard let name = dictionary["name"] as? String else { return nil }
        guard let description = dictionary["description"] as? String else { return nil }
        guard let thumbnail = dictionary["thumbnail"] as? JSONDictionary else { return nil }

        guard let thumbnailPath = thumbnail["path"] as? String else { return nil }
        guard let thumbnailExtension = thumbnail["extension"] as? String else { return nil }

        guard let stories = dictionary["stories"] as? JSONDictionary else { return nil }
        guard let storiesItems = stories["items"] as? [JSONDictionary] else { return nil }
        let storiesArray = storiesItems.flatMap(Story.init)

        guard let comics = dictionary["comics"] as? JSONDictionary else { return nil }
        guard let comicsItems = comics["items"] as? [JSONDictionary] else { return nil }
        let comicsArray = comicsItems.flatMap(Comics.init)

        guard let series = dictionary["series"] as? JSONDictionary else { return nil }
        guard let seriesItems = series["items"] as? [JSONDictionary] else { return nil }
        let seriesArray = seriesItems.flatMap(Series.init)

        self.id = id
        self.name = name
        self.description = description
        self.thumbnailPath = thumbnailPath
        self.thumbnailExtension = thumbnailExtension
        self.stories = storiesArray
        self.comics = comicsArray
        self.series = seriesArray
    }
}

extension Hero {

    static let all = Resource<(items: [Hero], total: Int)>(url: url!,
                                    queryItems: queryItems,
                                    parse: { data in

        let json = try? JSONSerialization.jsonObject(with: data as Data, options: [])

        guard let dictionary = json as? JSONDictionary else { return nil }

        guard let data = dictionary["data"] as? JSONDictionary,
            let total = data["total"] as? Int else {
            return nil
        }

        guard let results = data["results"] as? [JSONDictionary] else {
            return nil
        }

        return (results.flatMap(Hero.init), total)
    })
}

extension Hero {

    var media: Resource<Media> {

        let meadiaURL = URL(string: "\(thumbnailPath)/\(Constants.APIDetails.APIImageType).\(thumbnailExtension)")!

        return Resource<Media>(url: meadiaURL, queryItems: [], parse: { data in
            let image = UIImage(data: data) ?? UIImage()
            return Media(image: image)
        })
    }
}
