//
//  Constants.swift
//  Marvel
//
//  Created by woroninb on 25/05/2018.
//  Copyright © 2018 com.roche. All rights reserved.
//

import Foundation

enum Constants {

    enum APIDetails {
        static let TimeStamp = "ts"
        static let Hash = "hash"
        static let APIKey = "apikey"
        static let APIPath = "/v1/public/characters"
        static let APIImageType = "portrait_xlarge"

        static let offset = 0
        static let offsetDelta = 20
    }

    enum InfoPlistKeys {
        static let PrivateKey = "privateKey"
        static let ApiKey = "apiKey"
        static let Url = "url"
    }
}
