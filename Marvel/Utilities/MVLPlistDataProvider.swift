//
//  MVLDataProvider.swift
//  Marvel
//
//  Created by woroninb on 25/05/2018.
//  Copyright © 2018 com.roche. All rights reserved.
//

import Foundation
import CryptoSwift

enum MVLPlistDataProvider {

    static func valueForInfoListKey(_ key: String) -> AnyObject? {
        if let infoDictionary = Bundle.main.infoDictionary {
            return infoDictionary[key] as AnyObject?
        }
        return nil
    }

    static func apiKey() -> String {
        guard let key = valueForInfoListKey(Constants.InfoPlistKeys.ApiKey) as? String else { return "" }
        return key
    }

    static func privateKey() -> String {
        guard let key = valueForInfoListKey(Constants.InfoPlistKeys.PrivateKey) as? String else { return "" }
        return key
    }

    static func url() -> String {
        guard let key = valueForInfoListKey(Constants.InfoPlistKeys.Url) as? String else { return "" }
        return key
    }

    static func timestampHash() -> (timestamp: String, hash: String) {

        let timestamp = Date().toString(dateFormat: "yyyy-MM-dd HH:mm:ss")
        return (timestamp, (timestamp + privateKey() + apiKey()).md5())

    }
}
