//
//  MarvelStrings.swift
//  Marvel
//
//  Created by woroninb on 25/05/2018.
//  Copyright © 2018 com.roche. All rights reserved.
//

import Foundation

enum MVLStrings {
    enum MarvelListVC {
        static let title = NSLocalizedString("Marvel", comment: "")
        static let searchPlaceholder = NSLocalizedString("Search hero...", comment: "")
    }

    enum MarvelDetailsVC {
        static let title = NSLocalizedString("Details", comment: "")
    }
}
