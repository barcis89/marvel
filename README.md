## Marvel iOS app

![Marvel App](https://bitbucket.org/barcis89/marvel/raw/master/Marvel.gif)

### General

- Application was build upon MVC architecture
- Separated files into 6 folders:
	- Controllers
	- Views
	- Utillities
	- Network
	- Model
	- Extensions
- Networking is build upon *URLSesion* as a *Webservice* class
- UI is implemented in code (without Storyboard) with usage of Autolayout
- Used *swiftlint* for support code readibility
- Used *CryptoSwift* Pod for MD5 encryption

### Environment

- Xcode 9.0
- Swift 3

### To Do

- Add functionality for marking items as a favorite
- Add Core Data for storing favorite items
- Add error handling for Network
- Add custom transition between *ListViewController* and *DetailsViewController*
- Add unit tests
- Add UI tests